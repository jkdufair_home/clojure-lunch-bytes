(ns tictactoe.core)

(def markers ["X" "O"])

(def one-row-won ["X" "X" "X" "O" nil "O" nil nil nil])
(def one-column-won ["O" nil "X" "O" nil "X" "O" nil nil])
(def no-win ["X" "X" nil "O" nil "O" nil nil "X"])
(def diagonal-backslash-won ["X" "O" nil nil "X" "O" nil nil "X"])
(def diagonal-forward-slash-won [nil "O" "X" "O" "X" nil "X" nil nil])
(def first-move ["X" nil nil nil nil nil nil nil nil])

(declare prompt-move game-over)

(defn make-board [size]
	(vec (take (* size size) (repeat nil))))

(defn render-row [row]
	(clojure.string/join
	 "|"
	 (map #(if (not %) " " %) row)))

(defn width [board]
	(int (Math/sqrt (count board))))

(defn rows [board]
	(let [board-width (width board)]
		(partition board-width board)))

(defn columns [board]
	(apply map vector (rows board)))

(defn diagonals [board]
	(let [skip-forward-slash (dec (width board))
				skip-backslash (inc (width board))]
		(list
		 ;; forward slash diagonal
		 (as-> skip-forward-slash $
			(drop $ board)
			(drop-last skip-forward-slash $)
			(take-nth skip-forward-slash $))
		 	;; backslash diagonal
		 (take-nth skip-backslash board))))

(defn display-board [board]
	(let [board-width (width board)
				board-rows	(rows board)]
		(doseq [indexed-row (map-indexed (fn [idx row] [idx row]) board-rows)]
			(println (render-row (second indexed-row)))
			(when-not (= (first indexed-row) (- board-width 1))
				(println (clojure.string/join (repeat (+ board-width (- board-width 1)) "-")))))))

(defn all-equal-marker [marker marked-positions]
	(and (every? #(= marker %) marked-positions)
			 marker))

(defn marked-positions-winner [marked-positions]
	(some identity (map #(all-equal-marker % marked-positions) markers)))

(defn is-any-row-won? [board]
	(some identity (map #(marked-positions-winner %)
											(concat
											 (rows board)
											 (columns board)
											 (diagonals board)))))

(defn valid-moves [board]
	(map inc
			 (map first
						(filter #(= (second %) nil) (map-indexed vector board)))))

(defn valid-move? [board position]
	(nil? (nth board position)))

(defn next-marker [board]
	(if (even? (count (filter identity board)))
		"X"
		"O"))

(defn make-move
  [board position]
	(if (valid-move? board position)
		(assoc board position (next-marker board))))

(defn get-input
  "Waits for user to enter text and hit enter, then cleans the input"
  ([] (get-input nil))
  ([default]
     (let [input (clojure.string/trim (read-line))]
       (if (empty? input)
         default
         (clojure.string/lower-case input)))))

(defn user-entered-invalid-move
  "Handles the next step after a user has entered an invalid move"
  [board]
  (println "\n!!! That was an invalid move :(\n")
  (prompt-move board))

(defn user-entered-valid-move
  "Handles the next step after a user has entered a valid move"
  [board]
  (if (empty? (valid-moves board))
    (game-over board)
    (prompt-move board)))

(defn prompt-move
  [board]
	(if (is-any-row-won? board)
		(game-over board)
		(do
			(println "\nHere's your board:")
			(display-board board)
			(println
			 (str "Player " (next-marker board) ", choose a blank square ('exit' to quit):"))
			(let [input (get-input)]
				(if (= input "exit")
					(game-over board)
					(if-let [new-board (make-move board (dec (Integer. input)))]
						(user-entered-valid-move new-board)
						(user-entered-invalid-move board)))))))

(defn prompt-size
  []
  (println "What size? [3]")
  (let [size (Integer. (get-input 3))
        board (make-board size)]
    (prompt-move board)))

(defn game-over
  "Announce the game is over and prompt to play again"
  [board]
  (let [winner (is-any-row-won? board)]
    (println "Game over!")
		(if winner
			(println (str "The winner is " winner ".")))
		(if (empty? (valid-moves board))
			(println "Cat's game."))
    (println "Play again? y/n [y]")
    (let [input (get-input "y")]
      (if (= "y" input)
        (prompt-size)
        (do
          (println "Bye!"))))))
